variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com"
}

variable "gitlab_project_id" {
  type        = string
  description = "43167169"
}

variable "gitlab_namespace_path" {
  type        = string
  description = "smathur-gl"
}

variable "gcp_project_name" {
  type        = string
  description = "smathur-4de34871"
}
